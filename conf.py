cluster = 'slony'

master_host = 'localhost'
master_port = 5432
master_user = 'myuser'
master_password = 'mypassword'
master_db = 'mydb'

slave_host = '192.168.1.3'
slave_port = 5432
slave_user = 'myremoteuser'
slave_password = 'myremotepassword'
slave_db = 'myremotedb'

master_pid_file = '/tmp/slon-master.pid'
slave_pid_file = '/tmp/slon-slave.pid'

master_log_file = '/tmp/slon-master.log'
slave_log_file = '/tmp/slon-slave.log'

master_conf_file = '/tmp/slon-master.conf'
slave_conf_file = '/tmp/slon-slave.conf'
