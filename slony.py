import os
import sys
import re
from optparse import OptionParser
from sqlalchemy import create_engine
from sqlalchemy.sql.expression import text
from time import sleep
import signal
import commands
import imp


def run(s):
    print(s)
    if os.system(s) != 0:
        sys.exit()

def copy_db_structure():
    filename = 'structure.sql'
    command = 'pg_dump -U {master_user} -h {master_host} -p {master_port:d} ' + \
              '{master_db} -f {filename} --no-owner --schema-only'
    command = command.format(master_user=conf.master_user,
                master_host=conf.master_host, master_port=conf.master_port,
                master_db=conf.master_db, filename=filename)
    run(command)
    command = 'psql -U {slave_user} -h {slave_host} -p {slave_port:d} ' + \
              '{slave_db} -f {filename}'
    command = command.format(slave_user=conf.slave_user,
                slave_host=conf.slave_host, slave_port=conf.slave_port,
                slave_db=conf.slave_db, filename=filename)
    run(command)
    os.remove(filename)

def slonik(s):
    print(s)
    filename = 'last-slonik-script.txt'
    f = open(filename, 'w')
    f.write(s)
    f.close()
    run('slonik ' + filename)
    os.remove(filename)

def get_conn(db, host, port, user, password):
    return 'dbname={db} host={host} port={port:d} user={user} password={password}'.format(
            db=db, host=host, port=port, user=user, password=password)

def get_master_conn():
    return get_conn(conf.master_db, conf.master_host, conf.master_port,
                    conf.master_user, conf.master_password)

def get_slave_conn():
    return get_conn(conf.slave_db, conf.slave_host, conf.slave_port,
                    conf.slave_user, conf.slave_password)
 
def get_slonik_conn():
    master = get_master_conn()
    slave = get_slave_conn()
    return '''
cluster name={cluster};
node 1 admin conninfo = '{master}';
node 2 admin conninfo = '{slave}';
'''.format(cluster=conf.cluster, master=master, slave=slave)

def get_slonik_cluster():
    conn = get_slonik_conn()
    master = get_master_conn()
    slave = get_slave_conn()
    return '''
{conn}
init cluster (id=1, comment='Master node');
store node (id=2, comment='Slave node', event node=1);
store path (server=1, client=2, conninfo='{master}');
store path (server=2, client=1, conninfo='{slave}');
'''.format(conn=conn, master=master, slave=slave)

def get_slonik_init_subscribe():
    conn = get_slonik_conn()
    master = get_master_conn()
    slave = get_slave_conn()
    return '''
{conn}
subscribe set (id=1, provider=1, receiver=2, forward=no);
'''.format(conn=conn, master=master, slave=slave)

def create_cluster():
    s = get_slonik_cluster()
    slonik(s)

def get_slonik_execute(filename):
    fullpath = os.path.realpath(filename)
    conn = get_slonik_conn()
    return '''
{conn}
execute script (
    filename = '{fullpath}',
    event node = 1
);
'''.format(conn=conn, fullpath=fullpath)

def repair(id=1, node=1):
    conn = get_slonik_conn()
    s = '''
{conn}
repair config (
    set id = {id}, 
    event node = {node} 
);'''.format(conn=conn, id=id, node=node)
    slonik(s)
    
def repair_all():
    sql = "SELECT set_id FROM _slony.sl_set ORDER BY 1"
    q = engine.execute(sql)
    set_ids = []
    for r in q.fetchall():
        set_ids.append(r.set_id)
    for node_id in [1, 2]:
        for set_id in set_ids:
            repair(set_id, node_id)

def execute_file(filename):
    s = get_slonik_execute(filename)
    slonik(s)

def slon(conf_file, background=False, log_file=None):
    suffix = background and ' 2>&1 &' or ''
    if log_file:
        suffix = ' >%s' % log_file + suffix
    command = '/usr/local/bin/slon -f %s%s' % (conf_file, suffix)
    run(command)

def run_master(background=False, with_log=False):
    c = get_master_conf()
    c.write(conf.master_conf_file)
    slon(conf.master_conf_file, background, with_log and conf.master_log_file)

def run_slave(background=False, with_log=False):
    c = get_slave_conf()
    c.write(conf.slave_conf_file)
    slon(conf.slave_conf_file, background, with_log and conf.slave_log_file)

def get_schema():
    return '_' + conf.cluster

def get_set_id():
    sql = "SELECT max(set_id) AS last_id FROM {schema}.sl_set".format(
            schema=get_schema())
    q = engine.execute(sql)
    r = q.fetchone()
    last_id = r.last_id or 0
    return last_id + 1

sql_table = """
SELECT c.oid, n.nspname, c.relname
  FROM pg_catalog.pg_class c
  LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
  WHERE c.relname = :table_name
    AND pg_catalog.pg_table_is_visible(c.oid)
  ORDER BY 2, 3
"""

sql_table_schema = """
SELECT c.oid, n.nspname, c.relname
  FROM pg_catalog.pg_class c
  LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
  WHERE c.relname = :table_name
    AND n.nspname = :schema
  ORDER BY 2, 3
"""

sql_fields = """
SELECT a.attname,
  pg_catalog.format_type(a.atttypid, a.atttypmod),
  (SELECT substring(pg_catalog.pg_get_expr(d.adbin, d.adrelid) for 128)
     FROM pg_catalog.pg_attrdef d
     WHERE d.adrelid = a.attrelid
       AND d.adnum = a.attnum
       AND a.atthasdef),
  a.attnotnull, a.attnum,
  (SELECT c.collname
     FROM pg_catalog.pg_collation c, pg_catalog.pg_type t
     WHERE c.oid = a.attcollation
       AND t.oid = a.atttypid
       AND a.attcollation <> t.typcollation) AS attcollation,
  NULL AS indexdef,
  NULL AS attfdwoptions
  FROM pg_catalog.pg_attribute a
  WHERE a.attrelid = :table_id AND a.attnum > 0 AND NOT a.attisdropped
  ORDER BY a.attnum"""

def table_seq(table_name): 
    t = table_name.split('.')
    if t[1:]:
        schema = t[0]
        table_name = t[1]
        sql = text(sql_table_schema)
        q = engine.execute(sql, schema=schema, table_name=table_name)
    else:
        sql = text(sql_table)
        q = engine.execute(sql, table_name=table_name)
    r = q.fetchone()
    table_id = r.oid
    sql = text(sql_fields)
    q = engine.execute(sql, table_id=table_id)
    regex = re.compile("nextval\('(.*)'\:")
    for r in q.fetchall():
        if not r.substring:
            continue
        if r.substring.find('nextval') == -1:
            continue
        match = regex.search(r.substring)
        return match.group(1)


class Set(object):
    def __init__(self, comment=None):
        self.comment = comment
        self.id = get_set_id()
        self.objs = []

    def add(self, name, typ='table'):
        t = name.split('.')
        if not t[1:]:
            name = 'public.' + name
        s = "set add {typ} (set id={id}, fully qualified name = '{name}');".format(
                typ=typ, id=self.id, name=name) 
        self.objs.append(s)

    def get_script(self):
        conn = get_slonik_conn()
        s_comment = self.comment and ", comment='%s'" % self.comment or ''
        create = 'create set (id=%d, origin=1%s);' % (self.id, s_comment)
        s_objs = '\n'.join(self.objs)
        lines = [conn, create, s_objs]
        if self.id == 1:
            forward = ", forward=no"
        else:
            forward = ""
        subscribe = 'subscribe set (id=%d, provider=1, receiver=2%s);' % (
            self.id, forward)
        merge = 'merge set (id=1, add id=%d, origin=1);' % self.id
        lines += [subscribe, merge]
        return '\n'.join(lines)

    def add_tables(self, tables):
        for table in tables:
            self.add(table)
            seq_name = table_seq(table)
            if seq_name:
                self.add(seq_name, 'sequence')

    def add_sequences(self, sequences):
        for sequence in sequences:
            self.add(sequence, 'sequence')


def delete_nodelock(eng):
    sql = "DELETE FROM _slony.sl_nodelock"
    eng.execute(sql)
 

class SlonConf(object):
    def __init__(self, db, host, port, user, password, pid_file):
        self.db = db
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.pid_file = pid_file

    def get_script(self):
        conn = get_conn(self.db, self.host, self.port, self.user, self.password)
        s = """# Generated by {this} 
cluster_name='{cluster}'
conn_info='{conn}'
pid_file='{pid_file}'
log_timestamp=true
log_timestamp_format='%Y-%m-%d %H:%M:%S %Z '
"""
        return s.format(this=sys.argv[0], cluster=conf.cluster, conn=conn,
            pid_file=self.pid_file)

    def write(self, filename): 
        s = self.get_script()
        f = open(filename, 'w')
        f.write(s)
        f.close()

def get_master_conf():
    return SlonConf(conf.master_db, conf.master_host, conf.master_port,
        conf.master_user, conf.master_password, conf.master_pid_file)

def get_slave_conf():
    return SlonConf(conf.slave_db, conf.slave_host, conf.slave_port,
            conf.slave_user, conf.slave_password, conf.slave_pid_file)

def is_live(pid_file):
    if not os.path.exists(pid_file):
        return
    f = open(pid_file)
    pid = int(f.read())
    f.close()
    try:
        os.kill(pid, 0)
    except OSError:
        return
    for i in range(3):
        if commands.getoutput('ps ax | grep "^%s%d " | grep -v grep' % (
            ' '*i, pid)):
            return pid

def stop_daemon(pid_file):
    pid = is_live(pid_file)
    if not pid:
        sys.exit()
    print('kill %d by signal' % pid)
    os.kill(pid, signal.SIGTERM)
    i = 0
    while i < 5:
        sleep(1)
        i += 1
        if not is_live(pid_file):
            sys.exit()
    print('kill %d by force' % pid)
    os.kill(pid, signal.SIGKILL)
    sys.exit()


pars = OptionParser()
pars.add_option('', '--copy-db-structure', action='store_true')
pars.add_option('', '--create-cluster', action='store_true')
pars.add_option('', '--run-master', action='store_true')
pars.add_option('', '--run-slave', action='store_true')
pars.add_option('', '--background', action='store_true',
    help='Run with background mode')
pars.add_option('', '--stop-master', action='store_true')
pars.add_option('', '--stop-slave', action='store_true')
pars.add_option('', '--add-table')
pars.add_option('', '--add-seq')
pars.add_option('', '--comment')
pars.add_option('', '--delete-nodelock', action='store_true')
pars.add_option('', '--without-log', action='store_true')
pars.add_option('', '--execute-file')
pars.add_option('', '--repair', action='store_true')
pars.add_option('', '--conf', default='conf.py',
    help='default: conf.py')

option, remain = pars.parse_args(sys.argv[1:])

if not os.path.exists(option.conf):
    print('%s not found' % option.conf)
    sys.exit(1)

conf = imp.load_source('conf', option.conf)


INSTRUCTION = '''
1. Use --copy-db-structure
2. Use --create-cluster
3. Use --run-master
4. Use --run-slave
5. Use --add-table, --add-seq for subscribe data

Other options use --help'''


db_url = 'postgresql://{user}:{password}@{host}:{port:d}/{dbname}'.format(
            user=conf.master_user, password=conf.master_password,
            host=conf.master_host, port=conf.master_port,
            dbname=conf.master_db)
engine = create_engine(db_url) 

if option.copy_db_structure:
    copy_db_structure()
    sys.exit()

if option.create_cluster:
    create_cluster()
    sys.exit()

if option.execute_file:
    execute_file(option.execute_file)
    sys.exit()

if option.repair:
    repair_all()
    sys.exit()

if option.run_master:
    with_log = not option.without_log
    run_master(option.background, with_log)
    sys.exit()

if option.run_slave:
    with_log = not option.without_log
    run_slave(option.background, with_log)
    sys.exit()

if option.stop_master:
    stop_daemon(conf.master_pid_file)
    sys.exit()

if option.stop_slave:
    stop_daemon(conf.slave_pid_file)
    sys.exit()
    
if option.delete_nodelock:
    delete_nodelock(engine)
    db_url = 'postgresql://{user}:{password}@{host}:{port:d}/{dbname}'.format(
                user=conf.slave_user, password=conf.slave_password,
                host=conf.slave_host, port=conf.slave_port,
                dbname=conf.slave_db)
    engine_slave = create_engine(db_url) 
    delete_nodelock(engine_slave)
    sys.exit()

if option.add_table or option.add_seq:
    if option.add_table:
        tables = option.add_table.split(',')
    if option.add_seq:
        seqs = option.add_seq.split(',')
    set_ = Set(option.comment)
    if option.add_table:
        set_.add_tables(tables)
    if option.add_seq:
        set_.add_sequences(seqs)
    s = set_.get_script()
    slonik(s)
    sys.exit()

print(INSTRUCTION)
