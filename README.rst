Slony Helper
============

Make sure time zone abbreviations work correctly::

  $ sudo su
  # su - postgres
  $ psql -c "SELECT timeofday()::timestamp with time zone"

If you get an error message, add the time zone to use in the file
``/usr/share/postgresql/9.3/timezonesets/Default``::

  WIB     25200    # Waktu Indonesia Barat
  WITA    28800    # Waktu Indonesia Tengah
  WIT     32400    # Waktu Indonesia Timur

You can get other time zone information in ``/usr/share/postgresql/9.3/timezonesets/*``.

Then restart postgresql service::

  $ sudo service postgresql restart

Install the required packages::

  $ sudo apt-get install build-essential postgresql-server-dev-9.3
  $ sudo apt-get install python-sqlalchemy python-psycopg2

Download Slony 2.2.x from http://slony.info, and then compile::

  $ tar xfvj slony1-2.2.0.tar.bz2
  $ cd slony1-2.2
  $ ./configure --with-perltools
  $ make
  $ sudo make install
  $ sudo cp src/slon/slon src/slonik/slonik /usr/local/bin

Create slave database at remote host.

Adjust ``conf.py``, and copy database structure to slave::

  $ python slony.py --copy-db-structure

Create _slony schema at both master and slave::

  $ python slony.py --create-cluster

Run master daemon::

  $ python slony.py --run-master

Run slave daemon::

  $ python slony.py --run-slave

Register the tables that will be duplicated::

  $ python slony.py --add-table=table1,table2

All done.

Reference: http://www.linuxjournal.com/article/7834
